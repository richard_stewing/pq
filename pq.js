//1.2.2013 by Richard Stewing 
// solves quadratic equations passed as a string form : x*x(+)px(+)q

//adds eventlistener for the app
onload = function (){
	document.getElementById("butt").onclick = calculate;

}

var pq = function (equation){
	/*var req = new XMLHttpRequest();
	req.open("PUT", 'http://188.109.205.9:8080/', true);
	req.onload = function (res){ 
		if(req.readyState === 4) {
			console.log(req.responseText);
		}
	};
	var json = {
		data : "test",
		date : new Date()

	};

	req.send(JSON.stringify(json));*/

	// create local varibels
	var p, q, d, that;

	// extracts p and q from the sring 
	p = sliceToP(equation);
	q = sliceToQ(equation);
	that = {};
	

	// creates d
	d = ((p/2)*(p/2))-q;

	// starts to give the return object "that" the props
	that.p = p;
	that.q = q;
	that.d = d;

	//using the pq formula rules 
	//if d ist smaller than 0
	if(d<0){
		that.solution = "doesn't exist";
		that.solutionCount = 0;
		console.log(that);
		return that;

	//if d is equal 0
	}else if(d === 0){
		that.solution = -1*(p/2);
		that.solutionCount = 1;
		console.log(that);
		return that;


	// if d is bigger than 0 
	}else {
		that.solution = {};
		that.solution.x1 = -1*(p/2)+Math.sqrt(d);
		that.solution.x2 = -1*(p/2)+(-1*Math.sqrt(d));
		that.solutionCount = 2;
		console.log(that);
		return that;
	}
	
};



var sliceToP = function (eq){
	var linPart, posTimes;
	//gives the linear part of the function 
	linPart = eq.slice(4);
	// finds position of the remaining x to extrect p
	posTimes = linPart.search("x");
	return parseFloat(linPart.slice(0, posTimes));
};


var sliceToQ = function (eq){
	var linPart, posPlus;
	// gives linear part
	linPart = eq.slice(4);
	// finds position of the remaining x plus 2 to extrect q
	posPlus = linPart.search("x")+2;
	return parseFloat(linPart.slice(posPlus));
};


//process the calculations with the consrtoctor above
function calculate(){
	//console.log(document.getElementById("eq").value);
	// creates the needed speziefic object
  	var sl = new pq(document.getElementById("eq").value);
  			

  	//diffrent diplay style for difftren solutions
  	if(sl.solutionCount === 0)
  		document.getElementById("sol").innerHTML = "There are no answers!!! Dis is smaller 0 :" +sl.d;
  	if(sl.solutionCount === 1)
  		document.getElementById("sol").innerHTML = "There is only one answere : " + sl.solution + ", the dis is equal 0 :" + sl.d ;
  	if(sl.solutionCount === 2)
  		document.getElementById("sol").innerHTML = "There are two answere : " + sl.solution.x1 + " and " + sl.solution.x2 + ", d is :" + sl.d;


  	
    console.log(sl);

  			



};
